package com.example.demo.Service;

import com.example.demo.Entity.Category;
import com.example.demo.Entity.Delivery;
import com.example.demo.Entity.Driver;
import com.example.demo.Repository.DeliveryRepository;
import com.example.demo.Repository.DriverRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class DriverService {
    private final DriverRepository driverRepository;

    public DriverService(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    public  ResponseEntity<?> getAll(){
        return  ResponseEntity.ok(driverRepository.findAll());
    }
    public  ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(driverRepository.findById(id));
    }

    public void delete(long id){
        driverRepository.deleteById(id);
    }
    public Driver update(@RequestBody Driver driver){
        return  driverRepository.save(driver);
    }
}
