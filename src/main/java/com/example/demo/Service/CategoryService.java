package com.example.demo.Service;

import com.example.demo.Entity.Category;
import com.example.demo.Entity.Factory;
import com.example.demo.Entity.Order;
import com.example.demo.Repository.CategoryRepository;
import com.example.demo.Repository.FactoryRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> getAll(){

        return (List<Category>)categoryRepository.findAll();
    }
    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(categoryRepository.findById(id));
    }

    public void delete(long id){
        categoryRepository.deleteById(id);
    }
    public Category update(@RequestBody Category category){
        return  categoryRepository.save(category);
    }
    public List<String> getNames(){
        return categoryRepository.findName();
    }

}
