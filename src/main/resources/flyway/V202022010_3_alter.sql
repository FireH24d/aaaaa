ALTER TABLE "handlings" ADD CONSTRAINT "handlings_pk" PRIMARY KEY ("id");
ALTER TABLE "factory" ADD CONSTRAINT "factory_pk" PRIMARY KEY ("id");
ALTER TABLE "delivery" ADD CONSTRAINT "delivery_pk" PRIMARY KEY ("id");
ALTER TABLE "driver" ADD CONSTRAINT "driver_pk" PRIMARY KEY ("id");
ALTER TABLE "category" ADD CONSTRAINT "category_pk" PRIMARY KEY ("id");
ALTER TABLE "product" ADD CONSTRAINT "product_pk" PRIMARY KEY ("id");
ALTER TABLE "orderr" ADD CONSTRAINT "order_pk" PRIMARY KEY ("id");
ALTER TABLE "login" ADD CONSTRAINT "login_pk" PRIMARY KEY ("id");
ALTER TABLE "shop" ADD CONSTRAINT "shop_pk" PRIMARY KEY ("id");
ALTER TABLE "payment" ADD CONSTRAINT "payment_pk" PRIMARY KEY ("id");
ALTER TABLE "order_item" ADD CONSTRAINT "order_item_pk" PRIMARY KEY ("id");
ALTER TABLE "status" ADD CONSTRAINT "status_pk" PRIMARY KEY ("id");



ALTER TABLE "handlings" ADD CONSTRAINT "handlings_fk0" FOREIGN KEY ("factory_id") REFERENCES "factory"("id");
ALTER TABLE "handlings" ADD CONSTRAINT "handlings_fk1" FOREIGN KEY ("order_id") REFERENCES "orderr"("id");
ALTER TABLE "handlings" ADD CONSTRAINT "handlings_fk2" FOREIGN KEY ("delivery_id") REFERENCES "delivery"("id");


ALTER TABLE "delivery" ADD CONSTRAINT "delivery_fk0" FOREIGN KEY ("driver_id") REFERENCES "driver"("id");



ALTER TABLE "product" ADD CONSTRAINT "product_fk0" FOREIGN KEY ("category_id") REFERENCES "category"("id");

ALTER TABLE "order_item" ADD CONSTRAINT "order_item_fk0" FOREIGN KEY ("order_id") REFERENCES "orderr"("id");
ALTER TABLE "order_item" ADD CONSTRAINT "order_item_fk1" FOREIGN KEY ("product_id") REFERENCES "product"("id");

ALTER TABLE "orderr" ADD CONSTRAINT "order_fk0" FOREIGN KEY ("shop_id") REFERENCES "shop"("id");
ALTER TABLE "orderr" ADD CONSTRAINT "order_fk1" FOREIGN KEY ("status") REFERENCES "status"("id");

ALTER TABLE "login" ADD CONSTRAINT "login_fk0" FOREIGN KEY ("shop_id") REFERENCES "shop"("id");


ALTER TABLE "payment" ADD CONSTRAINT "payment_fk0" FOREIGN KEY ("shop_id") REFERENCES "shop"("id");
